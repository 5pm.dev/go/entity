package entity

import (
	"context"
	"fmt"
	"time"

	"github.com/google/uuid"
)

// reserved metadata names
const (
	MetadataAllowContact string = "_allow_contact"
)

// internalMetadata a list of metadata that is internal use only for easy access
var internalMetadata = map[string]bool{
	MetadataAllowContact: true,
}

// Metadata a detail for an entity
type Metadata struct {
	// UUID *uuid.UUID `json:"uuid,omitempty"`

	CreatedAt  *time.Time `json:"created_at,omitempty"`
	DataType   DataType   `json:"data_type,omitempty"`
	EntityUUID *uuid.UUID `json:"entity_uuid,omitempty"`
	Internal   bool       `json:"internal,omitempty"`
	Name       string     `json:"name,omitempty"`
	UpdatedAt  *time.Time `json:"updated_at,omitempty"`
	Value      MetaValue  `json:"value,omitempty"`

	// system fields that are only used for processing purposes
	isNew     bool `json:"-"`
	isUpdated bool `json:"-"`
}

// MetaValue the value of a metadata
type MetaValue struct {
	Previous *PreviousMetaValue `json:"previous,omitempty"`
	Value    any                `json:"value"`
}

// PreviousMetaValue details of the previous value for the metadata
type PreviousMetaValue struct {
	DataType DataType `json:"data_type"`
	Value    any      `json:"value"`
}

// NewMetadata creates a new metadata
func NewMetadata(
	ctx context.Context,
	name string,
	value any,
) *Metadata {
	// get the data type
	dataType := checkDataType(ctx, value)

	// coerce the value to a string if the data type is default
	var metaValue MetaValue
	if dataType == DataTypeDefault {
		dataType = DataTypeString
		metaValue = MetaValue{Value: fmt.Sprintf("%v", value)}
	} else {
		metaValue = MetaValue{Value: value}
	}

	return &Metadata{
		Name:     name,
		Value:    metaValue,
		DataType: dataType,
		Internal: isInternal(name),

		isNew: true,
	}
}

// Update a metadata if the value or type has changed
func (m *Metadata) Update(ctx context.Context, v MetaValue) error {
	// get the data type of the new value
	dataType := checkDataType(ctx, v.Value)

	var changed bool

	if m.DataType == dataType {
		if m.DataType == DataTypeArray {
			if !SlicesEqual(m.Value.Value.([]any), v.Value.([]any)) {
				changed = true
			}
		} else {
			if m.Value.Value != v.Value {
				changed = true
			}
		}
	} else {
		changed = true
	}

	// if the value has changed then update the metadata
	if changed {
		t := time.Now()

		// move the old data to the retainer
		previousValue := PreviousMetaValue{
			DataType: m.DataType,
			Value:    m.Value.Value,
		}

		// set the value to the new value
		m.Value.Previous = &previousValue
		m.Value.Value = v.Value
		m.UpdatedAt = &t
		m.DataType = dataType
		m.isUpdated = true
	}

	return nil
}

// IsNew returns if the metadata is new or not based on its flag or missing uuid
func (m *Metadata) IsNew() bool {
	return m.isNew // || m.UUID == nil || *m.UUID == uuid.Nil
}

// SetIsNew sets the new flag
func (m *Metadata) SetIsNew(isNew bool) {
	m.isNew = isNew
}

// IsUpdated returns if the metadata has been update or not
func (m *Metadata) IsUpdated() bool {
	return m.isUpdated
}

// SetIsUpdated sets the updated flag
func (m *Metadata) SetIsUpdated(isUpdated bool) {
	m.isUpdated = isUpdated
}

// isInternal checks to see if a name is used for internal purposes
func isInternal(name string) bool {
	if internal, ok := internalMetadata[name]; ok {
		return internal
	}

	return false
}
