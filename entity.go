package entity

import (
	"context"
	"errors"
	"strings"
	"time"

	"github.com/google/uuid"
)

var (
	ErrMergeFailedMismatchedTypes = errors.New("entities are not the same type")
	ErrMergeFailedMismatchedKeys  = errors.New("entity keys don't match")
	ErrMergeFailedNewEntityNil    = errors.New("new entity is nil")
	ErrMergeFailedNoMetadata      = errors.New("no metadata to merge")
	ErrMergeFailedOriginEntityNil = errors.New("origin entity is nil")
)

// EntityType defines the different types of entities that are supported
type EntityType string

const (
	EntityTypeOrganization EntityType = "ORGANIZATION"
	EntityTypePerson       EntityType = "PERSON"
)

// String returns the string representation of the entity type
func (et EntityType) String() string {
	return string(et)
}

// LowerString returns the lower case string representation of the entity type
func (et EntityType) LowerString() string {
	return strings.ToLower(string(et))
}

// Entity defines an entity with all its details
type Entity struct {
	UUID       *uuid.UUID           `json:"uuid,omitempty"`
	CreatedAt  *time.Time           `json:"created_at,omitempty"`
	UpdatedAt  *time.Time           `json:"updated_at,omitempty"`
	Domain     string               `json:"domain,omitempty"`
	Email      string               `json:"email,omitempty"`
	EntityType EntityType           `json:"entity_type,omitempty"`
	Metadata   map[string]*Metadata `json:"metadata,omitempty"`
}

// AddMetadata adds a metadata to the entity
func (e *Entity) AddMetadata(
	ctx context.Context,
	data ...*Metadata,
) (updated bool, err error) {
	// make sure the map is initialized
	if e.Metadata == nil {
		e.Metadata = make(map[string]*Metadata)
	}

	// loop over the data and add/update the entity metadata
	for i := range data {
		metadata := data[i]

		if existing, found := e.Metadata[metadata.Name]; found {
			if err := existing.Update(ctx, metadata.Value); err != nil {
				continue
			}

			// make sure to set updated to true if the metadata hs been updated
			if existing.IsUpdated() {
				updated = true
			}
		} else {
			e.Metadata[metadata.Name] = metadata
			updated = true
		}
	}

	return updated, nil
}

// Merge the given entity into the orginal entity
func (e *Entity) Merge(ctx context.Context, n *Entity) error {
	// if the orginal entity is nil just set the whole thing to the given entity
	if e == nil || e.UUID == nil || *e.UUID == uuid.Nil {
		return ErrMergeFailedOriginEntityNil
	}

	// make sure the new entity is not nil
	if n == nil {
		return ErrMergeFailedNewEntityNil
	}

	// should not merge entities that are not the same type so error
	if e.EntityType != n.EntityType {
		return ErrMergeFailedMismatchedTypes
	}

	// keys should match or the details are not for the same entity
	if (e.EntityType == EntityTypeOrganization && e.Domain != n.Domain) || (e.EntityType == EntityTypePerson && e.Email != n.Email) {
		return ErrMergeFailedMismatchedKeys
	}

	// no point in merging if there is no medatada to merge
	if len(n.Metadata) == 0 {
		return ErrMergeFailedNoMetadata
	}

	// pull the metadata from the new entity into a slice
	var m []*Metadata
	for _, v := range n.Metadata {
		m = append(m, v)
	}

	// merge all the metadata from the given entity into the original entity
	if _, err := e.AddMetadata(ctx, m...); err != nil {
		return err
	}

	return nil
}
