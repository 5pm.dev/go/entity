package entity

import (
	"context"
	"testing"

	"github.com/google/uuid"
)

func TestAddMetadata(t *testing.T) {
	ctx := context.Background()

	tests := [3]struct {
		name           string
		metadata       []*Metadata
		expectedUpdate bool
		expectedError  error
	}{
		// add a new metadata
		{
			name: "add new metadata",
			metadata: []*Metadata{
				NewMetadata(ctx, "testName", "stringValue"),
				NewMetadata(ctx, "testName2", NewRange(1, 10)),
			},
			expectedUpdate: true,
			expectedError:  nil,
		},

		// update existing metadata
		{
			name: "update exiting metadata",
			metadata: []*Metadata{
				NewMetadata(ctx, "testName", 9),
			},
			expectedUpdate: true,
			expectedError:  nil,
		},

		// nothing to update
		{
			name: "not updating exiting metadata",
			metadata: []*Metadata{
				NewMetadata(ctx, "testName", 9),
			},
			expectedUpdate: true,
			expectedError:  nil,
		},
	}

	entity := Entity{}

	for _, test := range tests {
		updated, err := entity.AddMetadata(ctx, test.metadata...)
		if err != test.expectedError {
			t.Errorf("test %s: expected error %v, got %v", test.name, test.expectedError, err)
		}
		if updated != test.expectedUpdate {
			t.Errorf("test %s: expected update %v, got %v", test.name, test.expectedUpdate, updated)
		}
	}
}

func TestMerge(t *testing.T) {
	ctx := context.Background()

	uuid1 := uuid.New()

	tests := [1]struct {
		name          string
		entity1       *Entity
		entity2       *Entity
		expectedError error
	}{
		// successfully merge entiteis
		{
			name: "success",
			entity1: &Entity{
				UUID:       &uuid1,
				Domain:     "test.com",
				EntityType: EntityTypeOrganization,
				Metadata: map[string]*Metadata{
					"test_field": NewMetadata(ctx, "test_field", "stringValue"),
					"test_array": NewMetadata(ctx, "test_array", []any{"test1", "test2"}),
				},
			},
			entity2: &Entity{
				Domain:     "test.com",
				EntityType: EntityTypeOrganization,
				Metadata: map[string]*Metadata{
					"test_field": NewMetadata(ctx, "test_field", "newStringValue"),
					"test_array": NewMetadata(ctx, "test_array", []any{"test1", "test2", "test3"}),
				},
			},
			expectedError: nil,
		},

		// @TODO: add more test cases to cover everything
	}

	for _, test := range tests {
		err := test.entity1.Merge(ctx, test.entity2)
		if err != test.expectedError {
			t.Errorf("test %s: expected error %v, got %v", test.name, test.expectedError, err)
		}
	}
}
