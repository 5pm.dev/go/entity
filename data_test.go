package entity

import (
	"context"
	"testing"
)

func TestCheckDataType(t *testing.T) {
	tests := [12]struct {
		name         string
		value        any
		expectedType DataType
	}{
		{
			name:         "array",
			value:        []any{1, 2, 3, "test"},
			expectedType: DataTypeArray,
		},
		{
			name:         "bool",
			value:        true,
			expectedType: DataTypeBoolean,
		},
		{
			name:         "string",
			value:        "test",
			expectedType: DataTypeString,
		},
		{
			name:         "int",
			value:        1,
			expectedType: DataTypeInteger,
		},
		{
			name:         "int8",
			value:        int8(1),
			expectedType: DataTypeInteger,
		},
		{
			name:         "int16",
			value:        int16(1),
			expectedType: DataTypeInteger,
		},
		{
			name:         "int32",
			value:        int32(1),
			expectedType: DataTypeInteger,
		},
		{
			name:         "int64",
			value:        int64(1),
			expectedType: DataTypeInteger,
		},
		{
			name:         "float32",
			value:        float32(1),
			expectedType: DataTypeFloat,
		},
		{
			name:         "float64",
			value:        float64(1),
			expectedType: DataTypeFloat,
		},
		{
			name:         "range",
			value:        NewRange(1, 10),
			expectedType: DataTypeRange,
		},
		{
			name:         "default",
			value:        struct{}{},
			expectedType: DataTypeDefault,
		},
	}

	for _, test := range tests {
		dataType := checkDataType(context.Background(), test.value)
		if dataType != test.expectedType {
			t.Errorf("test %s: expected type %v, got %v", test.name, test.expectedType, dataType)
		}
	}
}

func TestNewRange(t *testing.T) {
	tests := [2]struct {
		name          string
		from          any
		to            any
		expectedRange Range
	}{
		{
			name: "int range",
			from: 1,
			to:   10,
			expectedRange: Range{
				Start: 1,
				End:   10,
			},
		},
		{
			name: "alpha range",
			from: "a",
			to:   "z",
			expectedRange: Range{
				Start: "a",
				End:   "z",
			},
		},
	}

	for _, test := range tests {
		r := NewRange(test.from, test.to)
		if r.Start != test.expectedRange.Start {
			t.Errorf("test %s: expected start %v, got %v", test.name, test.expectedRange.Start, r.Start)
		}
		if r.End != test.expectedRange.End {
			t.Errorf("test %s: expected end %v, got %v", test.name, test.expectedRange.End, r.End)
		}
	}
}
