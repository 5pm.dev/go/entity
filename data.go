package entity

import (
	"context"
	"strings"
	"time"
)

// DataType defines the different types of data that are supported
type DataType string

const (
	// default is used when the type is not defined, so we can coerce it to the correct type
	DataTypeDefault DataType = "DEFAULT"

	DataTypeArray   DataType = "ARRAY"
	DataTypeBoolean DataType = "BOOLEAN"
	DataTypeDate    DataType = "DATE"
	DataTypeFloat   DataType = "FLOAT"
	DataTypeInteger DataType = "INTEGER"
	DataTypeRange   DataType = "RANGE"
	DataTypeString  DataType = "STRING"
)

// String returns the string representation of the entity type
func (dt DataType) String() string {
	return string(dt)
}

// LowerString returns the lower case string representation of the entity type
func (dt DataType) LowerString() string {
	return strings.ToLower(string(dt))
}

// checkDataType checks the type of the value and returns the correct DataType
func checkDataType(ctx context.Context, value any) DataType {
	switch value.(type) {
	case []any:
		return DataTypeArray
	case bool:
		return DataTypeBoolean
	case string:
		return DataTypeString
	case int, int8, int16, int32, int64:
		return DataTypeInteger
	case float32, float64:
		return DataTypeFloat
	case Range:
		return DataTypeRange
	case time.Time:
		return DataTypeDate
	default:
		return DataTypeDefault
	}
}

// ------------------------------
// Define some custom types
// ------------------------------

// Range defines a type for a range of values
type Range struct {
	Start any `json:"start"`
	End   any `json:"end"`
}

// NewRange creates a new range
func NewRange(start, end any) Range {
	return Range{
		Start: start,
		End:   end,
	}
}
