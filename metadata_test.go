package entity

import (
	"context"
	"testing"
)

type testCustomType string

func TestNewMetadata(t *testing.T) {
	tests := [7]struct {
		name             string
		value            any
		expectedValue    any
		expectedDataType DataType
	}{
		{
			name:             "array",
			value:            []any{"test"},
			expectedValue:    []any{"test"},
			expectedDataType: DataTypeArray,
		},
		{
			name:             "string",
			value:            "test",
			expectedValue:    "test",
			expectedDataType: DataTypeString,
		},
		{
			name:             "int",
			value:            1,
			expectedValue:    1,
			expectedDataType: DataTypeInteger,
		},
		{
			name:             "int64",
			value:            int64(1),
			expectedValue:    int64(1),
			expectedDataType: DataTypeInteger,
		},
		{
			name:             "float64",
			value:            float64(1),
			expectedValue:    float64(1),
			expectedDataType: DataTypeFloat,
		},
		{
			name:             "range",
			value:            NewRange(1, 10),
			expectedValue:    NewRange(1, 10),
			expectedDataType: DataTypeRange,
		},
		{
			name:             "default",
			value:            testCustomType("testCustomType"),
			expectedValue:    "testCustomType",
			expectedDataType: DataTypeString,
		},
	}

	for _, test := range tests {
		metadata := NewMetadata(
			context.Background(),
			test.name,
			test.value,
		)
		if metadata.Name != test.name {
			t.Errorf("test %s: expected name %s, got %s", test.name, test.name, metadata.Name)
		}
		if metadata.DataType != DataTypeArray {
			if metadata.Value.Value != test.expectedValue {
				t.Errorf("test %s: expected value %v, got %v", test.name, test.expectedValue, metadata.Value.Value)
			}
		}
		if metadata.DataType != test.expectedDataType {
			t.Errorf("test %s: expected data type %v, got %v", test.name, test.expectedDataType, metadata.DataType)
		}
	}
}
